import { Component, OnInit, ViewChild, ElementRef, ViewContainerRef } from '@angular/core';
import * as Application from 'application';
import * as Fs from 'file-system';
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";

import * as camera from 'nativescript-camera';
import { PhotoEditor, PhotoEditorControl } from 'nativescript-photo-editor';
import { ImageSource, fromFileOrResource } from 'tns-core-modules/image-source/image-source';
import * as Enums from 'ui/enums';
import { Image } from 'ui/image';
import { ImageAsset } from 'tns-core-modules/image-asset/image-asset';
import { CaptureDialogComponent } from '../capture-dialog/capture-dialog.component';

declare let android;
declare let java;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  picturePath = '';
  thumbPath = '';
  picturePrefix = '';

  overlay = false;

  pictureFolder: Fs.Folder = Fs.Folder.fromPath(Fs.path.join(Fs.knownFolders.documents().path, 'pictures'));
  // pictureFolder: Fs.Folder = Fs.Folder.fromPath(
  // Fs.path.join(android.os.Environment.getExternalStoragePublicDirectory(android.os.Environment.DIRECTORY_DOWNLOADS).toString(),
  //   'MyFolder'));

    constructor(
        private modalService: ModalDialogService,
        private viewContainerRef: ViewContainerRef) { }


    ngOnInit() {
    }

    public testDrawing() { // drawing button home
        console.log('test drawing');
        const imageSource = fromFileOrResource(this.picturePath);
        this.drawingPicture(imageSource); // save image source with drawing
    }

    public takePicture() { // take picture button home
        console.log('show modal !');
        const options: ModalDialogOptions = {
            fullscreen: true,
            viewContainerRef: this.viewContainerRef
        };
        const that = this;

        console.log('ICI !!');
        this.modalService
            .showModal(CaptureDialogComponent, options)
            .then((res) => {
                console.log('end', res.imageAsset, res.drawing);

                if (res.drawing) {
                    const source = new ImageSource();
                    source.fromAsset(res.imageAsset).then((imageSource: ImageSource) => {
                        that.drawingPicture(imageSource); // save image source with drawing
                    });
                } else {
                    // save image asset without drawing
                    let imageAsset = null;
                    if (Application.android) {
                        imageAsset = res.imageAsset.android;
                    } else {
                        imageAsset = res.imageAsset.ios;
                    }
                    that.savePictureAsset(imageAsset);

                    // save image asset as image source
                    const source = new ImageSource();
                    // source.fromAsset(res.imageAsset).then((imageSource: ImageSource) => {
                    //     that.savePictureSource(imageSource); // save image source with drawing
                    // });
                }


            });
    }

  public resize(initialPicturePath: string, newPicturePath: string, size: { // resize picture ios and android
    width?: number, height?: number, maxDim?: number
  }, quality = 100) {
    if (Application.ios) {
        // const initialBitmap = UIImage.imageWithContentsOfFile(this.getFileName(initialPicturePath));

        // let width; let height;
        // if (size.width && size.height) {
        //     width = size.width;
        //     height = size.height;
        // } else if (size.maxDim) {
        //     console.log(initialBitmap);
        //     console.log(initialPicturePath);
        //     console.log(this.getFileName(initialPicturePath));
        //     const maxPreviousDim = Math.max(initialBitmap.size.width, initialBitmap.size.height);
        //     const coeff = size.maxDim / maxPreviousDim;
        //     width = coeff * initialBitmap.size.width;
        //     height = coeff * initialBitmap.size.height;
        // } else {
        //     throw Error('size must contain width and height or maxDim property');
        // }

        // UIGraphicsBeginImageContextWithOptions({width: width, height: height}, false, 0.0);
        // initialBitmap.drawInRect(CGRectMake(0, 0, width, height));
        // let resultImage = UIGraphicsGetImageFromCurrentImageContext();
        // UIGraphicsEndImageContext();

        // let folder = Fs.Folder.fromPath(Fs.path.join(Fs.knownFolders.documents().path, 'pictures'));
        // const mutableinitialBitmap = BitmapFactory.makeMutable(resultImage);
        // const initialBitmapBis = BitmapFactory.asBitmap(mutableinitialBitmap);
        // let saved = initialBitmapBis.toImageSource().saveToFile(newPicturePath, 'jpeg');
    } else if (Application.android) {
        const initialBmOptions = new android.graphics.BitmapFactory.Options();
        const initialBitmap = android.graphics.BitmapFactory.decodeFile(initialPicturePath, initialBmOptions);

        let width; let height;
        if (size.width && size.height) {
            width = size.width;
            height = size.height;
        } else if (size.maxDim) {
            const maxPreviousDim = Math.max(initialBitmap.getWidth(), initialBitmap.getHeight());
            const coeff = size.maxDim / maxPreviousDim;
            width = coeff * initialBitmap.getWidth();
            height = coeff * initialBitmap.getHeight();
        } else {
            throw Error('size must contain width and height or maxDim property');
        }

        const newBitmap = android.graphics.Bitmap.createScaledBitmap(initialBitmap, width, height, true);

        const out = new java.io.FileOutputStream(newPicturePath);
        newBitmap.compress(android.graphics.Bitmap.CompressFormat.JPEG, quality, out);
        out.flush();
        out.close();

        initialBitmap.recycle();
        newBitmap.recycle();

        // manually launch garbage collector
        java.lang.Runtime.getRuntime().gc();
    }
  }

  public savePictureAsset(picture: string) { // save picture with asset (when no drawing) : doesn't work on ios
    const time = (new Date()).getTime();

    // names and folders
    const baseName = this.picturePrefix + new Date().getTime();
    const pictureName = baseName + '.' + Enums.ImageFormat.png;
    const thumbName = baseName + '_thumbnail.' + Enums.ImageFormat.png;

    console.log('image asset => :  ' + picture);

    console.log('temps 1 ' + ((new Date()).getTime() - time) / 1000 + 's');

    // save original image
    const picturePath = Fs.path.join(this.pictureFolder.path, pictureName);
    this.resize(picture, picturePath, { maxDim : 1200 });

    console.log('temps 2 ' + ((new Date()).getTime() - time) / 1000 + 's');

    const thumbPath = Fs.path.join(this.pictureFolder.path, thumbName);

    this.resize(picture, thumbPath, { maxDim : 360 }, 90);

    console.log('temps 3 ' + ((new Date()).getTime() - time) / 1000 + 's');

    this.picturePath = picturePath;
    this.thumbPath = thumbPath;
  }


  public savePictureSource(picture: ImageSource) { // save picture with source (when drawing) : work on ios and android
    const time = (new Date()).getTime();

    // names and folders
    const baseName = this.picturePrefix + new Date().getTime();
    const pictureName = baseName + '.' + Enums.ImageFormat.png;
    const thumbName = baseName + '_thumbnail.' + Enums.ImageFormat.png;

    console.log('image asset : ' + picture);

    console.log('temps 1 ' + ((new Date()).getTime() - time) / 1000 + 's');

    // save original image
    const picturePath = Fs.path.join(this.pictureFolder.path, pictureName);
    picture.saveToFile(picturePath, 'png');
    const thumbPath = Fs.path.join(this.pictureFolder.path, thumbName);
    picture.saveToFile(thumbPath, 'png');

    console.log('temps 2 ' + ((new Date()).getTime() - time) / 1000 + 's');

    this.resize(picturePath, picturePath, { maxDim : 1200 });
    this.resize(thumbPath, thumbPath, { maxDim : 360 }, 90);

    console.log('temps 3 ' + ((new Date()).getTime() - time) / 1000 + 's');

    this.picturePath = picturePath;
    this.thumbPath = thumbPath;
  }

  public drawingPicture(imageSource: ImageSource) { // drawing picture with image source : return imageSource
    const photoEditor = new PhotoEditor();
    const that = this;

    console.log('ORIG IMAGE: ', imageSource.height, imageSource.width);

    photoEditor.editPhoto({
        imageSource: imageSource,
        hiddenControls: [],
    }).then((newImage: ImageSource) => {
        console.log('NEW IMAGE: ', newImage.height, newImage.width);

        that.savePictureSource(newImage);
    }).catch((e) => {
        console.error(e);
    });
  }

}
