import { Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { CaptureComponent } from './capture/capture.component';

export const routes: Routes = [
  {
      path: '',
      redirectTo: '/home',
      pathMatch: 'full',
  },
  {
      path: 'home',
      component: HomeComponent,
  },
  {
    path: 'capture',
    component: CaptureComponent,
    },
];
