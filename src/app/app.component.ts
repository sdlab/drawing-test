import { Component } from '@angular/core';
import { registerElement } from 'nativescript-angular/element-registry';
import { CameraPlus } from '@nstudio/nativescript-camera-plus';
registerElement('CameraPlus', () => <any>CameraPlus);

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
}
