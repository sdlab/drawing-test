import { Component } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { CameraPlus } from '@nstudio/nativescript-camera-plus';
import { ImageAsset } from "tns-core-modules/image-asset/image-asset";
import { ImageSource } from "tns-core-modules/image-source/image-source";
import { PhotoEditor, PhotoEditorControl } from 'nativescript-photo-editor';
import * as Application from 'application';
import * as Fs from 'file-system';
import * as Enums from 'ui/enums';
import { template } from "@angular/core/src/render3";

declare let android;
declare let java;

@Component({
  templateUrl: './capture-dialog.component.html'
})
export class CaptureDialogComponent {

    public prompt: string;
    private cam: CameraPlus;
    private drawing: boolean;
    picturePath = '';
    thumbPath = '';
    picturePrefix = '';
    takePictureAsset: ImageAsset;

    pictureFolder: Fs.Folder = Fs.Folder.fromPath(
    Fs.path.join(android.os.Environment.getExternalStoragePublicDirectory(android.os.Environment.DIRECTORY_DOWNLOADS).toString(),
        'MyFolder'));

    constructor(private params: ModalDialogParams) {
        console.log('AAAAA');
        this.prompt = params.context.promptMsg;
    }

    public camLoaded(e: any): void {
        console.log('***** cam loaded *****');
        this.cam = e.object as CameraPlus;
    }

    public close(result: string) {
        console.log('close');
        this.params.closeCallback(result);
    }

    public photoCapturedEvent(e: any): void {
        console.log('PHOTO CAPTURED EVENT!!!');
        console.log(e.data);
        const that = this;
        this.params.closeCallback({
            imageAsset: e.data,
            drawing: this.drawing
        });
    }

    public imagesSelectedEvent(e: any): void {
        console.log('IMAGES SELECTED EVENT!!!');
        // console.log(e.data);
    }

    public toggleCameraEvent(e: any): void {
        console.log('camera toggled');
    }

    public takePictureNoDrawing() {
        console.log('take picture no drawing');
        this.drawing = false;
        this.cam.takePicture({ saveToGallery: true });
    }

    public takePictureWithDrawing() {
        this.drawing = true;
        console.log('take picture with drawing');
        this.cam.takePicture({ saveToGallery: true });
    }
}
